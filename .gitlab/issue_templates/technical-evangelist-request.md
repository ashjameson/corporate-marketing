# Request Details

## :ballot_box_with_check: Type of Task/Request
> For tracking purposes, please add the respective `TE-Task` label corresponding to the type of request you choose.


**Team/Personal Task:**
> Personal or team tasks that needs to be tracked, please add the `TE-Request-Type::Internal` label
* [ ] Content Creation
* [ ] CFP Submission
* [ ] Event Attendance (Meetup/Conference/etc.)
* [ ] Speaking Engagement
* [ ] LiveStream / Webcast
* [ ] Community Contribution


**External Requests:**
> Requests from other team members within GitLab, please add the `TE-Request-Type::External` label
* [ ]  Blog / Content Creation
* [ ]  Speaking Engagement (in-person)
* [ ]  Event Coverage (in-person)
* [ ]  Webcast
* [ ]  Media Coverage (interview, etc.)
* [ ]  CFP Submission Review
* [ ]  Content Review Request
* [ ]  Speaking Coaching
* [ ]  Other

## :page_with_curl: Request 

* Request description: `A Great Event`
* Requested by: `@you`
* Due Date: `2020-01-01`


If it's an event, please provide the following details, remove this section othewise:

* Event URL: `https://example.com`
* Location: `Virtual/Somewhere, Worldwide`
* Event Dates: `2020-01-01`

## :building_construction: Request Triage Information
> This information will help the Technical Evangelism team triage the request and understand it's impact when weighed against other requests in flight.

* Audience Description: `Who is this for?  What's the intended audience?`
* Expected Audience Size (live): `500`
* Expected Audience Size (post-creation online audience): `1200`
* Is this a new/nascent audience for GitLab (e.g. .NET developers)? `Yes/No`
* If this is a speaking session, will it be recorded and available for promotion? `Yes/No`

> If the need is within the next week, please also slack [`#tech-evangelism`](https://app.slack.com/client/T02592416/CMELFQS4B)

## :speaking_head: Request Details 
> Add as many details as possible about the need

# Post Request Completion

## Request Completion Details

* Date of Execution/Completion: `<Date>`
* Link to resource: `<Video, Link, Doc, etc.>`
* Other Relevant Details: 

## Metrics

*Please provide any available  Metrics or accessible link to metrics*

## TE CheckList

* [ ] Added to Pathfactory
* [ ] Metrics Added to TE Metrics Sheet
* [ ] Repurposed

/label ~"tech-evangelism" ~"TE-Request::New" ~"TE-Request-Type::External" ~"TE-Content::new-request"

/assign @abuango
