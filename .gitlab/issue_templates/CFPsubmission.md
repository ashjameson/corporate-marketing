*Title: CFP Submission: Event Name - Talk Title *


Link to CFP Meta Issue: `https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/[XXXX]`
> Please create a new issue if not existing, using [this issue template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta).

## Event Details

* Name of event: 
* Event website: 
* Location: 
* Event Date:
* CFP Deadline: 

## Speaker / Proposal  Details
* Speaker: 
* Title: 
* One sentence description: 
* Abstract (can be a link to GDoc):
* Speaker Bio: 
* Other Info (e.g. length, takeaways, target audience):

## Additional speaker information (for non-GitLabbers)
* Company name: 
* How big is your company?: 
* Title (Position):

## Submission Status
* [ ] Do you need help putting the CFP together? (Please add the `TE-CFP::Help` label)
* [ ] Do you need a review before submission? 
* [ ] Submitted to Conference
* [ ] Accepted / Rejected / Waitlist

## For accepted proposals 
* [ ] Added to [events page](https://about.gitlab.com/handbook/marketing/corporate-marketing/#for-gitlab-team-members-attending-events-speaking)
* [ ] Speaker brief + bio sent
* [ ] Request for slide support
* [ ] Slides reviewed by content and design
* [ ] Speaker final run through needed?
* [ ] Speaker slides sent (Deadline: )
* [ ] Promotion support (please detail how you want Corporate Marketing to promote the talk)

For your input please: @pritianka @abuango

/label ~Events ~Speaker ~SPIFF ~"mktg-status::plan" ~"tech-evangelism" ~"TE-CFP" ~"TE-CFP::Draft"
