### Request for review and approval of channel/technology partner-led announcement.

<!-- Requester please fill in all relevant sections. Some details may not be applicable. Please see the handbook for more information 
on requesting an announcement: https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/#when-to-reach-out-to-channel-marketing-for-gtm-support. --> 

If a channel/technology partner is interested in taking the lead on making a formal announcement around joining the GitLab Partner Program, please feel free to share the [customizeable press release template](https://drive.google.com/file/d/1XvGWSqo6uOVZTR1Co4Af7PSNSLsGTOWv/view?usp=sharing). This template is a recommended guide for partners to customize based on their specific relationship with GitLab and focus area(s). GitLab is happy to support the partner company’s announcement by providing a supporting quote from either Michelle Hodges (Channel Partners) or Brandon Jung (Technology Partners), quote attribution based on your partner track. Please follow the below steps:

##### To-Do's
**Step 1: `Channel Account Manager` or `GitLab DRI for Partner` to send press release template to partner for completion. ([G Doc](https://docs.google.com/document/d/12B7wwjeaU9FJqk8nT2obXS08IAJz4LAdtajeM_JMQjs/edit) or [PDF](https://drive.google.com/file/d/1XvGWSqo6uOVZTR1Co4Af7PSNSLsGTOWv/view?usp=sharing)) Once complete, send the below details and completed press release draft to @tinas and @cweaver1 via this issue template for GitLab internal reviews/approval. 
* [ ] Partner press release template completed and link to document included below. 

** Step 2: `PR Team Member` to route the partner press release through internal reviews and approval (add relevant approver names below).**
* [ ] Christina Weaver/CorpComms-PR 
* [ ] Tina Sturgis/Channel-Partner Marketing 
* [ ] Channel Account Manager or Alliances Team Member
* [ ] Michelle Hodges or Brandon Jung

**Step 3: `PR Team Member` to share approved press release draft including any suggested edits (within 7-10 business days of request date) to the partner company PR/marketing DRI and CC all GitLab DRIs on the email (channel marketing, CAM, etc). 
* [ ] Approved press release sent to partner for announcement coordination

------

### Details

#### Name of the channel/technology partner that would like to announce joining the GitLab Partner Program?: 

[add details here]

#### Has the partner contract been finalized and signed?

- [ ] Yes
- [ ] No [If no, please note that the partner agreement/contract must be signed before making a formal announcement.]

#### Link to the completed partner press release draft.

[Included Google Doc link to the press release draft here.]

#### What is the date that the partner wants to share the announcement, ideally?

[add details here]

#### Is this proposed announcement date tied to a specific industry/partner event? If so, what is the date of the event?

[add details here]

#### Who is the GitLab DRI for the announcement? 

[add details here]

#### Any additional GitLab SMEs who need to be a part of the review/approval process?

[add details here]

#### Who is the PR/marketing DRI at the partner company? Please share name and contact info for coordination on approvals and comms activities.

[Include name(s) and email(s)]

####  Link related issues and epics
   - [ ] Please link to all related issues and the epic

------

/label ~"corporate marketing" ~"channel marketing" ~"corp comms" ~"PR" ~"announcement" ~"mktg-status::plan" ~"channel" ~"Partner Mktg:: New Request"

/cc @TinaS @cweaver1 @nwoods1 @meganphelan @Fabzzz

/confidential
