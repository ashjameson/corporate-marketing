<!-- This section is to be used by anyone internally requesting GitLab sponsor a corporate event. Not sure what qualifies, please visit see the [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=drive_web&ouid=110206726155880220171). Please fill all the necessary information out. If you don’t need a section, feel empowered to delete it from the issue. Remember to always add Related Issues and Epics after you've created your issue so folks have context on what other issues connect to this work. Keep being awesome! 

* title the issue: `"Name of event", Virtual, Date of Event` (add them remove this line)
* Make the due date the day the event starts `"note- we need at least 6 weeks to do any events with marketing support`
* `Assign the Quarter the work will happen in, and Region tags to the issue`
-->

## :pencil: Meta Epic link

<!-- If this issue is connected to an Epic, link the above subhead for easy discover. If not, you can remove.  -->
* Add Epic Link Here
* [Event Planning Sheet](https://docs.google.com/spreadsheets/d/1rXzp1bLoiI-IJ86Jbe39Kif2iujQoWVNXsbxQisFwUY/edit#gid=1085564346) copy this doc and create own version for specific event

## :speaking_head_in_silhouette: Social request issue

<!-- If this issue is connected to a related social request issue, link the above subhead for easy discover. If not, you can remove.  -->

## :rolled_up_newspaper: PR request issue

<!-- If this issue is connected to a related PR request issue, link the above subhead for easy discover. If not, you can remove.  -->

## :construction_site: PMM Support
<!-- If this project needs PMM support [Request Strategic Marketing support for speaking, demos, customer speakers, and/or messaging](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request)- start new issue with this template. If not, you can remove.  -->


## :star: Finance issue

<!-- If this issue is connected to a related finance issue, link the above subhead for easy discover. If not, you can remove. -->
* [Budget Doc](https://docs.google.com/spreadsheets/d/1AvL2Rkwnr7pYtkMYXualfVVAJc5Irzo8Abiw9cybwdY/edit) - Copy template and start new budget for each event. Add to finance issue when you start it. `Budget Doc Naming Convention: "ISOdate_CampaignShortName_Budget". It is the responsiobilty of the DRI to keep the budget doc up-to-date and to notify their manager as soon as or if you think you will go over budget` 
* `Finance Tag(s)` - create the 'campaign tag' (using proper ISOdate_name, noting 37 character limit) then add into the associated budget line item. Each campaign that needs tracking should get it's own tag. 
**Budgeted costs (sponsorship + auxiliary cost (AV, booth, swag, travel, printed materials...)):** `DRI to fill in`
* [ ]  Event [evaluated and aligned to regional and company goals](https://about.gitlab.com/handbook/marketing/events/#how-we-evaluate-and-build-potential-events)
* [ ]  Spend added to [Non Headcount Budget Sheet](https://docs.google.com/spreadsheets/d/1WVWZjSF6f5jAFqHO4hXcV8mN975ITT4eXScSn0F_FU8/edit#gid=0)
* [ ]  Finance issue created in [Issues · GitLab.com / Finance · GitLab](https://gitlab.com/gitlab-com/finance/issues) + add budget sheet to finance issue 
* [ ]  Once contract signed- Issue status changed from Plan to WIP- `this triggers MPM to create epic and all relevant issues`
* [ ]  Invoice received. [Instructions on procure to pay](https://about.gitlab.com/handbook/finance/accounting/#procure-to-pay). 
* [ ]  Invoice paid
* **Event Goals:**
  * Goals for # of leads: 
  * Goals for pipeline created $$$: 
  * Number of meetings at event: 
  * registration goals/ attendance goals: 

## :bulb: Background/purpose/ Why should we do this?

<!-- Give a brief 1-2 sentence overview of why this issue exists. Justification for wanting to sponsor the event (please be as detailed as possible)  -->

## :map: Details and reach

<!-- Include details on why you're completing this work, how it impacts the business, and the potential reach if applicable. Include key dates as well.  -->
* **Official Event Name:** 
* **Date:**
* **Location:** 
* **Event Website:**
* **Expected Number of Attendees at the event:**
* **GitLab Staffing Needs:**
* **Dress Code/ Attire:**
* **GitLab Hosted Landing page (if applicable):**
* **Speakers (if applicable):**
* **SFDC Campaign:**
* **Attendee List/ Audience Demographics:**
* **If we went last year, link to SF.com campaign:** 

## :dart: Goals, audience and key messages

<!-- Add context on the goals for this work. Add key messages if applicable. This helps those who may be unfamiliar with your functional group have a better understanding of the work.  -->* **Sponsorship (if applicable):** outline level, benefits and cost breakdown.

`**Once this section is complete you can go into contracting. To beging budget approval and contract review start an issue in the [finance project](https://gitlab.com/gitlab-com/finance/issues) using the "vendor_contracts_marketing" tenmplate. Copying/pasting the 'campaign tag' from above. Be sure to include any concessions you have negotiated or discounts we are getting in step to of the template.**

## :notebook_with_decorative_cover: Event brief

<!-- outline details of our o=involvement/ commitment -->

## :writing_hand: Talk, workshop, panel, or keynote details

<!-- Remove this section if no event, keynote, or panel is involved.  -->

## :stopwatch: Due dates, DRI(s) and next steps/to-dos :ballot_box_with_check:
<!-- link to associated issues  -->

 ---
 `**Everything avove this section will be updated once the event has been approved. All this info is needed for budget approval and to move into contracting**`

`**Once this section is complete you can go into contracting. To beging budget approval and contract review start an issue in the [finance project](https://gitlab.com/gitlab-com/finance/issues) using the "vendor_contracts_marketing" tenmplate. Copying/pasting the 'campaign tag' from above. Be sure to include any concessions you have negotiated or discounts we are getting in step to of the template.**`

---

## :white_check_mark: MPM request checklist  

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? - YES/NO (please specify nurture stream) [see nurture options here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#top-funnel-nurture)
* Landing page creation (requires promotion plan) - YES/NO
* If yes for Landing page: Should this event be added to the [public events list](https://about.gitlab.com/events/) - YES/NO 
* Will the landing page contain a form for meeting/demo requests - YES/NO (if YES, choose meeting/demo)
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Will this event require sales-nominated workflow? - YES/NO
* Speaking session - YES/NO/MAYBE
* Workshop - YES/NO/MAYBE
* Alliances/Partner Marketing involved - YES/NO/MAYBE (if yes, must designate a DRI)
* Will this event include use of Marketing Development Funds (MDFs)? YES/NO
   * if yes, please tag Tina Sturgis here, and complete the section below
        * [ ]  MDF request completed by FMM and sent to Tina at least 1 quarter out
        * [ ]  event accepted for use of MDF (tag Tina here for her to approve)
        * [ ]  receipt submitted post event
        * [ ]  proof of payment submitted
        * [ ]  leads submitted 

/assign @aoetama @emily

/label ~Events ~"Field Marketing" ~"MktgOps - FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign" ~"Corporate Event" ~"mktg-status::plan" ~"META" ~"Virtual Event" ~Virtual
