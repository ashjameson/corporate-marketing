## Request support for contributed article

<!-- Lead time requirements: 1 working week for abstract, 2 weeks from accepted work date for draft -->

- **Publication:**
- **Submission deadline (date, time, timezone):**
- **Initiative/campaign:**
- **Word count:**
- **Subject matter expert (desired job title, area of expertise):** 
- **High-res photo of author required:** Y/N

### Proposal or Prompt

<!-- What is this article about? What are the requirements? --> 

## TODO

- [ ] Requestor completes all details of the form
- [ ] SME accepts 
- [ ] Work is accepted, writer is assigned, deadline added, and issue moves to `mktg-status::wip` @erica
- [ ] Writer interviews SME
- [ ] First draft shared
- [ ] SME reviews draft
- [ ] Final draft is shared with SME
- [ ] SME approves
- [ ] Article sent for publication 


/label  ~"Content Marketing" ~"Contributed Articles" ~"PR" ~"mktg-status::plan" ~"Stage::Awareness"

/assign @erica 