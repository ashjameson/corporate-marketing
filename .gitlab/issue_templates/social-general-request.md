### Request for organic (non-paid) social promotion
<!-- Requester: Please name this issue: `Organic Social: `Name of campaign or request` --> 

<!-- Requester please fill in all sections above the solid line. Some details may not be applicable. --> 
## Requester: Please acknowledge the following before filling out your request
* [ ]  I understand that all social requests need a minimum of **1 full week** between the request and the first published post date and that I cannot put in a request for the same week.
* [ ] I understand that if my request is not tied to a corporate marketing-focused timed event or campaign, that the publishing schedule is entirely at the discretion of the social team.
## 📬 STEP 1: For Requester <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not open the issue until you're ready to answer the following questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->
#### Details Details
1.  What is your request all about?

**[add details here]**

2.  Pertinent or other go live date(s):

**[add date(s) here]**

3.  Is there a landing page? If not, please note you'll need to provide this link in order for us to move forward.

**[add page link, other details, here]**

4.  What is the overall utm_campaign? - *webcast123*, i.e. *utm_campaign=webcast123*
<!-- The social media team will use [this tracking sheet](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) to create URLs that are tied to social, and your campaign by using UTMs. Please [review the guidelines in handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#url-tagging) &/or speak with the marketer who is managing your campaign to ensure you are not interrupting the reporting structure they have in place. -->

**[add utm_campaign here]**   
* [ ]  I'm not sure what my utm_campaign code is or there isn't one

5.  What objectives do you want users to follow? (Think clicks, sign ups, awareness, etc.)
<!-- In most cases, every objective is not an appropriate course of action. If your objectives include registrations, please use the social-event-request template. -->
* [ ]  Awareness
* [ ]  Clicks to a GitLab owned URL
* [ ]  Clicks to a 3rd party URL
* [ ]  A numerical goal of **[add goal here]** 
<!-- Please note that for the numerical goal catergory, you may be referred to paid social advertising depending on the specifics. -->

6. Are there existing assets to use on social or will you require custom designs?  <!-- links with cards, images, or videos -->
* [ ]  Existing Assets to use are located here: **[requester, insert link to related issue or repo for assets]**
* [ ]  I require custom assets for social
* [ ]  I'm not sure what I require (the social team will review with you in the comments below)

7. Are you considering or will be adding paid social advertising for this campaign? <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue -->
* [ ]  Yes, I'm requesting paid social advertising **[add link to paid social issue]**
* [ ]  No, I am not requesting paid social advertising
* [ ]  I'm not sure (the social team will help you determine the right path)

8.  Anything else we should know or could be helpful to know?

**[add additional thoughts here]**

9. Please link all related issues and epics

-----

## 📝 STEP 2: Social Team: To-Do's
<!-- Be sure there is enough detail with the info above. If an area is not pertinent (e.g. live coverage isn't necessary), please use a strikethrough <s> to <s> to cross out the text.-->
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please use Canva to accomplish. If this is tied to a larger campaign, please open a design issue, tag requester, and link as a related issue here.
* [ ]  Forwarding request to be paid social advertising only (direct requester to the paid social issue and close this one)

## ✍️ STEP 3: All: Drafts, suggestions, and schedule 
*Please work on these items in the comments below.*

## STEP 4 (optional, based on campaign, communcation, etc.): Requester: Please approve the suggested posts in the comments below 👇
* [ ]  I have approved the suggested copy for all posts discussed in the comments
*<!--  Note, we're not asking for stylistic approval, but on messaging and strategy. -->

## 🗓 STEP 5: Social Team: Scheduled posts
* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Comment on this issue with any need-to-know details

## 📭 Social Team: Once the posts available to add to Sprout are scheduled and live coverage is completed, you may close this issue.

/label ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"

/assign @social